import re


def _multiple_replace(mapping, text):
	pattern = "|".join(map(re.escape, mapping.keys()))
	return re.sub(pattern, lambda m: mapping[m.group()], str(text))


def convert_fa_numbers(input_str):
	mapping = {
		'۰': '0',
		'۱': '1',
		'۲': '2',
		'۳': '3',
		'۴': '4',
		'۵': '5',
		'۶': '6',
		'۷': '7',
		'۸': '8',
		'۹': '9',
		'.': '.',
	}
	return _multiple_replace(mapping, input_str)


print(convert_fa_numbers('۱۲'))
